# GitLab agent Helm chart

The official Helm chart for the client-side component (agentk) of the [GitLab agent for
Kubernetes](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/).

For the server-side component (KAS), see the official [GitLab Helm chart](https://gitlab.com/gitlab-org/charts/gitlab)

## Getting started

### Prerequisites

This chart requires Helm v3.6 or later.

### Install the latest stable release

```shell
helm repo add gitlab https://charts.gitlab.io
helm repo update
helm upgrade --install gitlab-agent gitlab/gitlab-agent \
    --set config.kasAddress='wss://kas.gitlab.example.com' \
    --set config.token='YOUR.AGENT.TOKEN'
```

### Upgrade an existing release

```shell
helm repo update
helm upgrade  gitlab-agent gitlab/gitlab-agent --reuse-values
```

### Customize

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| additionalLabels | object | `{}` | Additional labels to be added to all created objects |
| affinity | object | `{}` |  |
| config.kasAddress | string | `"wss://kas.gitlab.com"` |  |
| config.observability.enabled | bool | `true` |  |
| config.observability.tls.enabled | bool | `false` |  |
| config.observability.tls.secret | object | `{}` |  |
| extraArgs | list | `[]` | Add additional args settings to the pod. |
| extraEnv | list | `[]` | Add additional environment settings to the pod. Can be useful in proxy environments |
| extraVolumeMounts | list | `[]` | Add extra volume mounts |
| extraVolumes | list | `[]` | Add extra volumes |
| fullnameOverride | string | `""` |  |
| hostAliases | list | `[]` |  |
| image.pullPolicy | string | `"IfNotPresent"` |  |
| image.repository | string | `"registry.gitlab.com/gitlab-org/cluster-integration/gitlab-agent/agentk"` |  |
| image.tag | string | `""` | Overrides the image tag whose default is the chart appVersion. |
| imagePullSecrets | list | `[]` |  |
| initContainers | list | `[]` | Optional initContainers definition |
| maxSurge | int | `1` |  |
| maxUnavailable | int | `0` |  |
| nameOverride | string | `""` |  |
| nodeSelector | object | `{}` |  |
| podAnnotations."prometheus.io/path" | string | `"/metrics"` |  |
| podAnnotations."prometheus.io/port" | string | `"8080"` |  |
| podAnnotations."prometheus.io/scrape" | string | `"true"` |  |
| podLabels | object | `{}` |  |
| podSecurityContext | object | `{}` |  |
| priorityClassName | string | `""` |  |
| rbac.create | bool | `true` | Specifies whether RBAC resources should be created |
| replicas | int | `2` |  |
| resources | object | `{}` |  |
| securityContext | object | `{}` |  |
| serviceAccount.annotations | object | `{}` | Annotations to add to the service account |
| serviceAccount.create | bool | `true` | Specifies whether a service account should be created |
| serviceMonitor.enabled | bool | `false` | Specifies whether to create a ServiceMonitor resource for collecting Prometheus metrics |
| terminationMessagePolicy | string | `"FallbackToLogsOnError"` |  |
| tolerations | list | `[]` |  |

### Install from source

``` shell
git clone https://gitlab.com/gitlab-org/charts/gitlab-agent.git
cd gitlab-agent
helm upgrade --install gitlab-agent . \
    --set config.kasAddress='wss://kas.gitlab.example.com' \
    --set config.token='YOUR.AGENT.TOKEN'
```

## Development

See [`CONTRIBUTING.md`](./CONTRIBUTING.md) for our contributor license agreement and code of conduct.

### Publishing a new release

1. When applicable: Update `appVersion` in [`Chart.yaml`](./Chart.yaml) with the latest [upstream release](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/releases).
1. When changing any Helm chart resources (such as anything in [`templates`](./templates) or [`Chart.yaml`](./Chart.yaml)): Update `version` in [`Chart.yaml`](./Chart.yaml).
1. For a normal release: Create a tag `vX.Y.Z` where `X.Y.Z` is the `version` from `Chart.yaml` under https://gitlab.com/gitlab-org/charts/gitlab-agent
1. For a security release: Create a tag `vX.Y.Z` where `X.Y.Z` is the `version` from `Chart.yaml` under https://gitlab.com/gitlab-org/security/charts/gitlab-agent
   * **Note:** Security releases should be made in coordination with release managers. The GitLab agent has not had a security release yet, but see [the GitLab's security development workflow](https://gitlab.com/gitlab-org/release/docs/blob/master/general/security/developer.md) for the general process.

## Third-party trademarks

[Kubernetes](https://kubernetes.io/) is a registered trademark of The Linux Foundation.
